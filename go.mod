module gitlab.com/ubports/development/core/go-ldm

go 1.22

require (
	github.com/godbus/dbus/v5 v5.1.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
)

require (
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.1.0 // indirect
)

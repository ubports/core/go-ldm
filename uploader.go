/*
 * Copyright 2014 Canonical Ltd.
 * Copyright 2024 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * Authors:
 * Manuel de la Pena: manuel.delapena@canonical.com
 *
 * This file is part of lomiri-download-manager.
 *
 * lomiri-download-manager is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-download-manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Package ldm provides a go interface to work with the lomiri download manager
package ldm

import (
	"errors"
	"runtime"
	"strings"

	"github.com/godbus/dbus/v5"
)

const (
	UPLOAD_SERVICE           = "com.lomiri.applications.Uploader"
	UPLOAD_INTERFACE         = "com.lomiri.applications.Upload"
	UPLOAD_MANAGER_INTERFACE = "com.lomiri.applications.UploadManager"
)

// Upload is the common interface of an upload. It provides all the required
// methods to interact with an upload created by ldm.
type Upload interface {
	Progress() (uint64, error)
	Metadata() (map[string]string, error)
	SetThrottle(uint64) error
	Throttle() (uint64, error)
	AllowMobileUpload(bool) error
	IsMobileUpload() (bool, error)
	Start() error
	Cancel() error
	Started() chan bool
	UploadProgress() chan Progress
	Canceled() chan bool
	Finished() chan string
	Error() chan error
}

// FileUpload represents a single file being uploaded by ldm.
type FileUpload struct {
	conn       *dbus.Conn
	proxy      proxy
	path       dbus.ObjectPath
	signals    chan *dbus.Signal
	started    chan bool
	canceled   chan bool
	finished   chan string
	errors     chan error
	progress   chan Progress
	matchOpts  []dbus.MatchOption
}

func (upload *FileUpload) free() {
	upload.conn.RemoveMatchSignal(upload.matchOpts...)
	upload.conn.RemoveSignal(upload.signals)
	close(upload.signals)
}

func cleanUploadData(upload *FileUpload) {
	upload.free()
}

func newFileUpload(conn *dbus.Conn, path dbus.ObjectPath) (*FileUpload, error) {
	proxy := conn.Object(UPLOAD_SERVICE, path)
	matchOpts := []dbus.MatchOption{
		dbus.WithMatchPathNamespace(path),
		dbus.WithMatchInterface(UPLOAD_INTERFACE),
		dbus.WithMatchSender(UPLOAD_SERVICE),
	}
	err := conn.AddMatchSignal(matchOpts...)
	if err != nil {
		return nil, err
	}

	upload := &FileUpload{
		conn:     conn,
		proxy:    proxy,
		path:     path,
		signals:  make(chan *dbus.Signal),
		started:  make(chan bool),
		canceled: make(chan bool),
		finished: make(chan string),
		errors:   make(chan error),
		progress: make(chan Progress),
		matchOpts: matchOpts,
	}

	// deliver signals over respective channels
	go func() {
		for s := range upload.signals {
			i := strings.LastIndex(s.Name, ".")
			iface, member := s.Name[:i], s.Name[i+1:]
			if s.Sender != UPLOAD_SERVICE || s.Path != path || iface != UPLOAD_INTERFACE {
				continue
			}
			switch member {
			case "started":
				var started bool
				readSignalArgs(s, &started)
				upload.started <- started
			case "canceled":
				var canceled bool
				readSignalArgs(s, &canceled)
				upload.canceled <- canceled
			case "finished":
				var path string
				readSignalArgs(s, &path)
				upload.finished <- path
			case "error":
				var reason string
				readSignalArgs(s, &reason)
				upload.errors <- errors.New(reason)
			case "progress":
				var received uint64
				var total uint64
				readSignalArgs(s, &received, &total)
				upload.progress <- Progress{received, total}
			}
		}
		close(upload.started)
		close(upload.canceled)
		close(upload.finished)
		close(upload.errors)
		close(upload.progress)
	}()
	conn.Signal(upload.signals)

	runtime.SetFinalizer(upload, cleanUploadData)
	return upload, nil
}

// Process returns the process so far in uploading the file.
func (upload *FileUpload) Progress() (uint64, error) {
	return getUint64Value(upload.proxy, UPLOAD_INTERFACE + ".progress")
}

// Metadata returns the metadata that was provided at creating time to the upload.
func (upload *FileUpload) Metadata() (map[string]string, error) {
	return getMetadataMap(upload.proxy, UPLOAD_INTERFACE + ".metadata")
}

// SetThrottle sets the network throttle to be used in the upload.
func (upload *FileUpload) SetThrottle(throttle uint64) (error) {
	return setUint64Value(upload.proxy, UPLOAD_INTERFACE + ".setThrottle", throttle)
}

// Throttle returns the network throttle that is currently used in the upload.
func (upload *FileUpload) Throttle() (uint64, error) {
	return getUint64Value(upload.proxy, UPLOAD_INTERFACE + ".throttle")
}

// AllowMobileUpload returns if the upload is allowed to use the mobile
// connection.
func (upload *FileUpload) AllowMobileUpload(allowed bool) error {
	return upload.proxy.Call(UPLOAD_INTERFACE + ".allowMobileUpload", 0, allowed).Err
}

// IsMobileUpload returns if the upload will be performed over the mobile data
// connection.
func (upload *FileUpload) IsMobileUpload() (bool, error) {
	return getBoolValue(upload.proxy, UPLOAD_INTERFACE + ".isMobileUploadAllowed")
}

func (upload *FileUpload) Start() error {
	return upload.proxy.Call(UPLOAD_INTERFACE + ".start", 0).Err
}

// Cancel cancels an upload that was in process and deletes any local files
// that were created.
func (upload *FileUpload) Cancel() (err error) {
	return upload.proxy.Call(UPLOAD_INTERFACE + ".cancel", 0).Err
}

// Started returns a channel that will be used to communicate the started signals.
func (upload *FileUpload) Started() chan bool {
	return upload.started
}

// Canceled returns a channel that will be used to communicate the canceled signals.
func (upload *FileUpload) Canceled() chan bool {
	return upload.canceled
}

// Finished returns a channel that will ne used to communicate the finished signals.
func (upload *FileUpload) Finished() chan string {
	return upload.finished
}

// Error returns the channel that will be used to communicate the error signals.
func (upload *FileUpload) Error() chan error {
	return upload.errors
}

// UploadProgress returns a channel that will be used to communicate the progress
// signals.
func (upload *FileUpload) UploadProgress() chan Progress {
	return upload.progress
}

type UploadManager struct {
	conn  *dbus.Conn
	proxy dbus.BusObject
}

// NewUploadManager creates a new manager that can be used to create an upload
//  in the ldm daemon.
func NewUploadManager() (*UploadManager, error) {
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		return nil, err
	}

	proxy := conn.Object(UPLOAD_SERVICE, "/")
	d := UploadManager{conn, proxy}
	return &d, nil
}

func (man *UploadManager) CreateUpload(url string, file string, metadata map[string]interface{}, headers map[string]string) (upload Upload, err error) {
	var t map[string]dbus.Variant
	for key, value := range metadata {
		t[key] = dbus.MakeVariant(value)
	}
	s := struct {
		U  string
		F  string
		M  map[string]dbus.Variant
		HD map[string]string
	}{url, file, t, headers}
	var path dbus.ObjectPath
	c := man.proxy.Call(UPLOAD_MANAGER_INTERFACE + ".createUpload", 0, s)
	if c.Err != nil {
		return nil, c.Err
	}

	if err = readCallArgs(c, &path); err != nil {
		return nil, err
	}
	upload, err = newFileUpload(man.conn, path)
	return upload, err
}

func (man *UploadManager) CreateMmsUpload(url string, file string, hostname string, port int32) (upload Upload, err error) {
	var path dbus.ObjectPath
	c := man.proxy.Call(UPLOAD_MANAGER_INTERFACE + ".createMmsUpload", 0, url, file, hostname, port)
	if c.Err != nil {
		return nil, c.Err
	}

	if err = readCallArgs(c, &path); err != nil {
		return nil, err
	}
	upload, err = newFileUpload(man.conn, path)
	return upload, err
}
